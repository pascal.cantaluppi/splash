# JavaScript Captive Portal

Static HTML/JS/CSS Site for a Click-through Splash Page

<img src="https://gitlab.com/pascal.cantaluppi/splash/-/raw/main/public/img/splash.png" width="100" />

## Overview

Cisco Meraki provides cloud managed WiFi with the ability to host your own "Splash Page", which is a captive portal service for authenticating users to join the network. This concept is called an External Captive Portal (ExCaP).

## Installation

- Host the public directory files on a static webserver such as Apache, GitHub or Firebase, or use the included NodeJS express server.
- Configure the Meraki wireless SSID with a Click-Through splash page authentication
  - Meraki Dashboard --> Configure --> Splash Page: Click-through
- Add the domain address of the webserver to the "Walled Garden"
  - Meraki Dashboard --> Configure --> Access Control --> SSID:yourSSID --> Walled Garden.
  - Note: You will need to use the IP address instead of the domain name or contact Meraki Support to enable Walled Garden Domain Names
- Point the Meraki Splash page "Customer URL" to the HTML file. `https://splash.benedict.ipso.ch/`

  - Meraki Dashboard --> Configure --> Splash Page --> Custom URL: `https://splash.benedict.ipso.ch/`

- In the root directory of the project, run

```
npm start
```

- The server will host the project on port 5000.

## Sample URL paramater string

```
https://splash.benedict.ipso.ch/?base_grant_url=https%3A%2F%2Fn143.network-auth.com%2Fsplash%2Fgrant&user_continue_url=http%3A%2F%2Fask.com%2F&node_id=149624921787028&node_mac=88:15:44:50:0a:94&gateway_id=149624921787028&client_ip=10.110.154.195&client_mac=60:e3:ac:f7:48:08:22
```

## Screenshot

![alt screenshot](screenshot.png)
